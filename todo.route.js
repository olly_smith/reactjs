import express from "express";
import todoHandle from "../handlers/todo.handle";

//* root: localhost:4000/api/todos

const router = express.Router();
/**
 * @url localhost:4000/api/todos?sortBy=level&sortDir=asc&page=2&key=task&limit=2&filterBy=level&filterValue=1
 * @query optional: sortBy(default: _id), sortDir(default: asc), page(default = 1), limit(default = 4), key, filterBy, filterValue
 */
router.get("/", todoHandle.findAll);

router.get("/:id", todoHandle.findOne);
router.post("/", todoHandle.create);
router.put("/:id", todoHandle.update);
router.delete("/:id", todoHandle.delete);

exports.todoRoute = router;
