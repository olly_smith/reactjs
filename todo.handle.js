import Todo from "../models/todo.model";

export default {
  async findAll(req, res) {
    try {
      let { page = 1, limit = 4, key = '', sortBy = '_id', sortDir = 'asc' } = req.query;
      page = Number(page) - 1;
      limit = Number(limit);
      let condition = {
        'task': new RegExp(key, 'i')
      };
      if(req.query.filterBy && req.query.filterValue) {
        condition[req.query.filterBy] = req.query.filterValue;
      }
      const counts = await Todo.find(condition).countDocuments();
      const result = await Todo.find(condition).sort([[sortBy, sortDir]])
        .skip(page * limit)
        .limit(limit)
        .exec();
      return res.status(200).json({ counts, result });
    } catch (error) {
      return res.status(500).json([]);
    }
  },
  async findOne(req, res) {
    try {
      const { id } = req.params;
      const todo = await Todo.findOne({ _id: id });
      return res.json(todo);
    } catch (error) {
      return res.status(500).json({ status: false });
    }
  },
  async create(req, res) {
    try {
      const Todo = await Todo.create(req.body);
      return res.json({ status: true });
    } catch (error) {
      return res.status(500).json({ status: false });
    }
  },
  async update(req, res) {
    try {
      const { id } = req.params;
      await Todo.findByIdAndUpdate(id, req.body);
      return res.json({
        status: true
      });
    } catch (error) {
      return res.status(500).json({ status: false });
    }
  },
  async delete(req, res) {
    try {
      const { id } = req.params;
      await Todo.findByIdAndRemove(id, req.body);
      return res.json({ status: true });
    } catch (error) {
      return res.status(500).json({ status: false });
    }
  }
};
