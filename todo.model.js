import mongoose from "mongoose";

const todoSchema = mongoose.Schema({
  task: {
    type: String,
    required: true
  },
  level: {
    type: Number,
    required: true
  },
  
});

export default mongoose.model("Todo", todoSchema);
