import express from "express";
import mongoose from "mongoose";
import cors from 'cors';
import bodyParser from "body-parser";
import logger from "morgan";
import { todoRoute } from "./routes/todo.route";

mongoose.connect("mongodb://localhost:27017/manage-todo", {
  useNewUrlParser: true,
  useUnifiedTopology: true 
});

const app = express();
const PORT = process.env.PORT || 5000;


app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser({ extended: false }));
app.use(cors());

app.use("/api/todos", todoRoute);

app.use((req, res, next) => {
  const error = new Error("Not Found");
  error.status = 404;
  error.message = "Invalid route";
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  return res.json({
    error: {
      message: error.message
    }
  });
});

app.listen(PORT, () => {
  console.log("Server is running at PORT", PORT);
});
